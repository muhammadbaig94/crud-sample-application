package com.springboot.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.springboot.demo.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer>{
	
	List<Employee> findBydepartment(int department);
	//List<Employee> findByIdGreaterThan(int id);
	
	@Query("from Employee join Department where department=?1 order by employee_id")
	List<Employee> findBydepartment_idSorted(int department);

}
