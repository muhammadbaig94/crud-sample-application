package com.springboot.demo.dao;

import com.springboot.demo.model.Department;
import com.springboot.demo.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepository extends JpaRepository<Department, Integer> {
}
