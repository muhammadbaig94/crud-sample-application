package com.springboot.demo.controller;
import com.springboot.demo.dao.EmployeeRepository;
import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.annotation.RequestAction;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.ocpsoft.rewrite.faces.annotation.Deferred;
import org.ocpsoft.rewrite.faces.annotation.IgnorePostback;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.springboot.demo.model.Employee;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.List;

@Scope (value = "session")
@Component (value = "employeeList")
@ELBeanName(value = "employeeList")
@Join(path = "/", to = "/employee-list.jsf")
public class EmployeeListController {
    @Autowired
    private EmployeeRepository employeeRepository;

    private List<Employee> employees;
    private Employee employee = new Employee();
    @Deferred
    @RequestAction
    @IgnorePostback
    public void loadData() {
        employees = employeeRepository.findAll();
    }
    public String delete(Employee emp) {
        employeeRepository.delete(emp);
        employees = employeeRepository.findAll();
        return "product-list.xhtml?faces-redirect=true";
    }
    public void onEdit(RowEditEvent event) {
       // FacesMessage msg = new FacesMessage("Item Edited",((Employee) event.getObject()).getEmployee_id());
       // FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Item Cancelled");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        employees.remove((Employee) event.getObject());
    }
    public void edit(Employee employee) {
        this.employee = employee;
    }
    public List<Employee> getEmployees() {
        return employees;
    }
    /*public void onRowEdit(RowEditEvent<Employee> event) {
        FacesMessage msg = new FacesMessage("Product Edited", String.valueOf(event.getObject().getCode()));
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent<Employee> event) {
        FacesMessage msg = new FacesMessage("Edit Cancelled", String.valueOf(event.getObject().getCode()));
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();

        if (newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }*/
}
