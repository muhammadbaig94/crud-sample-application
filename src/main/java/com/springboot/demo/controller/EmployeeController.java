package com.springboot.demo.controller;

import java.util.List;


import com.springboot.demo.dao.DepartmentRepository;
import com.springboot.demo.model.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.springboot.demo.dao.EmployeeRepository;
import com.springboot.demo.model.Employee;
import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;

@Scope(value = "session")
@Component(value = "employeeController")
@ELBeanName(value = "employeeController")
@Join(path = "/employee", to = "/employee-form.jsf")
@ManagedBean
public class EmployeeController {
	
	@Autowired
	EmployeeRepository empRepo;
	@Autowired
	DepartmentRepository repoDepartment;
	//@Autowired
	private Employee employee = new Employee();
	@RequestMapping("/home")
	public String home() {
		return "home.jsp";
	}

	//**************************Basic CRUD Operations***********************

	//Create and Update
	public String save() {


		empRepo.save(employee);
		employee = new Employee();
		return "employee-list.xhtml?faces-redirect=true";

	}
	public Employee getEmployee() {
		return employee;
	}
	@RequestMapping("/addDepartment")
	public String addEmployee(Department entry) {


		repoDepartment.save(entry);
		return "home.jsp";
	}
	
	//Retrieve All Data
	@RequestMapping("/getAllEntries")
	public ModelAndView getAllJournalEntries() {
		ModelAndView mv= new ModelAndView();
		List<Employee> entries= empRepo.findAll();
		mv.addObject("entries",entries);
		mv.setViewName("getAllEntries.jsp");
		
		return mv;
	}
	
	//Retrieve Specific Data
	@RequestMapping("/getEntry")
	public ModelAndView getJournalEntry(@RequestParam int id) {
		ModelAndView mv = new ModelAndView();
		Employee entry = empRepo.findById(id).orElse(new Employee());
		mv.addObject("entry",entry);
		mv.setViewName("getEntry.jsp");
		
		return mv;
	}
	
	//Delete Data
	@RequestMapping("/deleteEntry")
	public String deleteJournalEntry(@RequestParam int id) {
		
		empRepo.deleteById(id);
		return "home.jsp";
	}
	
	//***************************More Complex Queries***********************
	
	//Find By Category
	@RequestMapping("/getEntriesByCategory")
	public ModelAndView getEntriesByCategory(@RequestParam int department) {
		
		ModelAndView mv = new ModelAndView();
		List<Employee> entries = empRepo.findBydepartment(department);
		mv.addObject("entries",entries);
		mv.setViewName("getEntriesByCategory.jsp");
		return mv;
		
	}
	
	//Find By Id Greater Than
	@RequestMapping("/getEntriesByIdGT")
	public ModelAndView getEntriesByIdGT(@RequestParam int id) {
		
		ModelAndView mv = new ModelAndView();
		//List<Employee> entries = repo.findByIdGreaterThan(id);
		//mv.addObject("entries", entries);
		mv.setViewName("getEntriesByIdGT.jsp");
		return mv;
		
	}
	
	//Find By Category but sorted by title
	@RequestMapping("/getEntriesByCategorySorted")
	public ModelAndView getEntriesByCategorySorted(@RequestParam int department_id) {
		
		ModelAndView mv = new ModelAndView();
		List<Employee> entries = empRepo.findBydepartment_idSorted(department_id);
		System.out.println(empRepo.findBydepartment_idSorted(department_id));
		mv.addObject("entries",entries);
		mv.setViewName("getEntriesByCategorySorted.jsp");
		return mv;
		
	}
	
	
}
